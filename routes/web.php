<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/cast', 'CastController@index')->name('cast.index');
Route::get('/cast/create', 'CastController@create')->name('cast.create');
Route::post('/cast', 'CastController@store')->name('cast.store');
Route::get('/cast/{cast_id}', 'CastController@show')->name('cast.show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit')->name('cast.edit');
Route::put('/cast/{cast_id}', 'CastController@update')->name('cast.update');
Route::delete('/cast/{cast_id}', 'CastController@destroy')->name('cast.delete');


Route::get('/game', 'GameController@index')->name('game.index');
Route::get('/game/create', 'GameController@create')->name('game.create');
Route::post('/game', 'GameController@store')->name('game.store');
Route::get('/game/{game_id}', 'GameController@show')->name('game.show');
Route::get('/game/{game_id}/edit', 'GameController@edit')->name('game.edit');
Route::put('/game/{game_id}', 'GameController@update')->name('game.update');
Route::delete('/game/{game_id}', 'GameController@destroy')->name('game.delete');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@utama')->name('utama');
