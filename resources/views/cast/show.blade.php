@extends('layouts.main')
@section('title','Detail Cast')
@section('content')
<div class="page-heading">
    <h1 class="page-title">Detail Cast</h1>
    
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Detail Cast Film</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body" style="">
          <table class="table table-striped">
            <tbody>
                <tr>
                    <td>Nama</td>
                    <td>: {{ $tampilCast->nama }}</td>
                </tr>
                <tr>
                    <td>Umur</td>
                    <td>: {{ $tampilCast->umur }}</td>
                </tr>
                <tr>
                    <td>Bio</td>
                    <td>: {{ $tampilCast->bio }}</td>
                </tr>
            </tbody>
          </table>
          <a href="{{ route('cast.index') }}" class="btn btn-secondary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
        </div>
    </div>
</div>
@endsection