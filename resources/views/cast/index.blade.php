@extends('layouts.main')
@section('title','Cast')
@section('content')
<div class="page-heading">
    <h1 class="page-title">Cast</h1>
    
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Cast Film</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body" style="">
            @if (session('sukses'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('sukses') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            @if (session('suksesEdit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('suksesEdit') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            @if (session('delete'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('delete') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            <a class="btn btn-primary mb-3" href="{{ route('cast.create') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Cast</a>
           <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Aksi</th>
                </tr>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $d->nama }}</td>
                        <td>{{ $d->umur }}</td>
                        <td>{{ $d->bio }}</td>
                        <td>
                            <a href="{{ route('cast.show', $d->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="{{route('cast.edit', $d->id)}}" class="btn btn-sm btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <form action="{{ route('cast.delete', $d->id) }}" method="POST" class="d-inline">
                                @csrf
                                @method('delete')
                                   <button class="btn btn-sm btn-danger " onclick="return confirm('Apakah anda ingin menghapus?')" style="cursor: pointer"><i class="fa fa-trash"></i></button>
                               </form>
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </thead>
            
           </table>
        </div>
    </div>
</div>
@endsection