@extends('layouts.main')
@section('title','Tambah Cast')
@section('content')
<div class="page-heading">
    <h1 class="page-title">Tambah Cast</h1>
    
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Tambah Cast Film</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body" style="">
           <form action="{{ route('cast.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" autofocus>
                @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                  </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{ old('umur') }}">
                @error('umur')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control @error('bio') is-invalid @enderror" id="bio" rows="3" name="bio">{{ old('bio') }}</textarea>
                @error('bio')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <button class="btn btn-primary" style="cursor: pointer"><i class="fa fa-floppy-o" aria-hidden="true"></i> Simpan</button>
            <a href="{{ route('cast.index') }}" class="btn btn-secondary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
           </form>
        </div>
    </div>
</div>
@endsection