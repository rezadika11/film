<!doctype html>

<html lang="en">

<head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <title>Detail Data</title>

</head>

<body>

    <h2>Detail Data Game</h2>

    <table class="table table-striped">
        <tbody>
            <tr>
                <td>Name </td>
                <td>: {{ $detail->name }}</td>
            </tr>
            <tr>
                <td>Gameplay </td>
                <td>: {{ $detail->gameplay }}</td>
            </tr>
            <tr>
                <td>Developer </td>
                <td>: {{ $detail->developer }}</td>
            </tr>
            <tr>
                <td>Year </td>
                <td>: {{ $detail->year }}</td>
            </tr>
        </tbody>
    </table>
    <a href="{{ route('game.index') }}" class="btn btn-secondary">Kembali</a>




    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous">
    </script>

</body>

</html>
