<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Cast::all();

        return view('cast.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ], [
            'nama.required' => 'Nama tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            'bio.required' => 'Bio tidak boleh kosong',
        ]);

        Cast::create($validateData);

        return redirect(route('cast.index'))->with('sukses', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tampilCast = Cast::find($id);

        return view('cast.show', compact('tampilCast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ], [
            'nama.required' => 'Nama tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            'bio.required' => 'Bio tidak boleh kosong',
        ]);

        $cast = Cast::find($id);
        $cast->update($validateData);

        return redirect(route('cast.index'))->with('suksesEdit', 'Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = cast::find($id);
        $cast->delete();
        return redirect(route('cast.index'))->with('delete', 'Data berhasil dihapus');
    }
}
