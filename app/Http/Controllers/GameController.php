<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Game::all();
        return view('game.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('game.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ], [
            'name.required' => 'Nama tidak boleh kosong',
            'gameplay.required' => 'Gameplay tidak boleh kosong',
            'developer.required' => 'Developer tidak boleh kosong',
            'year.required' => 'Year tidak boleh kosong',
        ]);

        Game::create($validateData);

        return redirect(route('game.index'))->with('sukses', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Game::find($id);

        return view('game.show', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Game::find($id);
        return view('game.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ], [
            'name.required' => 'Nama tidak boleh kosong',
            'gameplay.required' => 'Gameplay tidak boleh kosong',
            'developer.required' => 'Developer tidak boleh kosong',
            'year.required' => 'Year tidak boleh kosong',
        ]);

        $game = Game::find($id);
        $game->update($validateData);

        return redirect(route('game.index'))->with('sukses', 'Data berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $game = Game::find($id);
        $game->delete();
        return redirect(route('game.index'))->with('delete', 'Data berhasil dihapus');
    }
}
