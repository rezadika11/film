<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = 'kritik';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public function film()
    {
        return $this->belongsToMany('App\Film');
    }
}
