<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = 'peran';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function cast()
    {
        return $this->belongsTo('App\Cast');
    }

    public function film()
    {
        return $this->belongsTo('App\Cast');
    }
}
