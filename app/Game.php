<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'game';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'gameplay', 'developer', 'year',
    ];
}
