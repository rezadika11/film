<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function peran()
    {
        return $this->hasMany('App\Peran');
    }

    public function kritik()
    {
        return $this->hasMany('App\Kritik');
    }
}
